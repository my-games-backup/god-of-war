# GOD OF WAR PC

**Game Details**


![wallpaper](image.jpg)

* **System Information**
```
CPU       : AMD Ryzen 3 3100 4-Core Processor (8 CPUs), ~3.6GHz
RAM       : Ripjaws V DDR4-3400MHz CL16-18-18-38 1.35V 8GB Single Stick
SSD       : Walton Antique 256GB nvme m.2 SSD 
HDD       : ATA TOSHIBA DT01ACA0 500GB
MainBoard : Gigabyte B450M DS3H V2 Rev v1.0
GPU       : AMD RX 470 4GB 
OS 	      : Windows 10 Pro 21H2 
```
* **Save File Locatio**n <br>
  `%USERPROFILE%\Saved Games\God of War`

  Save File 
  
  1. `1638` old version of gow
  2. `2115807540` new version of gow and started game with `new game +`
  
* **Graphic Settings (Basic)**
```
VideoDevice=Radeon (TM) RX 470 Graphics
DisplayMode=Borderless
WindowWidth=1366
WindowHeight=768
AspectRatio=Auto
Vsync=Off                     
MotionBlur=6                  
FilmGrain=3                   
FSRQuality=UltraQuality
RenderScale=Off
TextureQuality=Original      
FilterQuality=Original       
ModelQuality=Original        
ShadowQuality=Original       
AtmosphereQuality=Original   
ReflectionQuality=Original   
OcclusionQuality=Original    
FPSLimit=60  
```
* **Controller Settings**
```
Mode=Default
Key=xbox360_controller
```
